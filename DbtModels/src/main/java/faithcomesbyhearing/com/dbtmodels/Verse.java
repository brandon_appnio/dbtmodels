package faithcomesbyhearing.com.dbtmodels;

public class Verse {

    public String damId;
    public String bookCode;
    public int chapterNumber;
    public int verseNumber;
    public String text;
}
