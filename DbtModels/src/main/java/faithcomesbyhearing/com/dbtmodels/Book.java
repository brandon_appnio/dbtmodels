package faithcomesbyhearing.com.dbtmodels;

public class Book {

    public String damId;
    public String name;
    public String chapterNumbers;
    public int numberOfChapters;
    public String bookCode;
    public int sortOrder;
}
