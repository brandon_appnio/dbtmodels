package faithcomesbyhearing.com.dbtmodels;

import java.util.List;

public class Volume {

    public List<String> damIds;
    public String languageCode;
    public String languageName;
    public String isoCode;
    public String mediaType;
    public String name;
    public String versionCode;

    @Override
    public boolean equals(Object o) {
        if (o instanceof Volume) {
            Volume volume = (Volume) o;
            return name.equals(volume.name);
        }
        return false;
    }
}
